import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home.vue'
import DirectiveExample from '@/components/DirectiveExample.vue'
import PropsExample from '@/components/PropsExample.vue'
import LifeCycleHookExample from '@/components/LifeCycleHookExample.vue'
import NestedRountingExample from '@/components/NestedRountingExample.vue'
import NestedChild1 from '@/components/NestedChild1.vue'
import NestedChild2 from '@/components/NestedChild2.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'Home', component: Home },
    { path: '/directives', name: 'DirectiveExample', component: DirectiveExample },
    { path: '/props', name: 'PropsExample', component: PropsExample },
    { path: '/life-cycle-hook', name: 'LifeCycleHookExample', component: LifeCycleHookExample },
    { path: '/nested-routing', name: 'NestedRountingExample', component: NestedRountingExample, children: [
      { path: 'child1', name: 'NestedChild1', component: NestedChild1 },
      { path: 'child2', name: 'NestedChild2', component: NestedChild2 }
    ]}
  ]
})

export default router
